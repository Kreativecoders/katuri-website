// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs

//= require "jquery.carouFredSel-6.2.1-packed.js"
//= require "jquery.touchSwipe.min.js"
//= require "jquery.elevateZoom-2.5.5.min.js"
//= require "jquery.imagesloaded.min.js"
//= require "jquery.themepunch.plugins.min.js"
//= require "jquery.themepunch.revolution.min.js"
//= require "jquery.appear.js"
//= require "jquery.sparkline.min.js"
//= require "jquery.easy-pie-chart.js"
//= require "jquery.easing.1.3.js"
//= require "jquery.fancybox.pack.js"
//= require "jquery.isotope.min.js"
//= require "jquery.knob.js"
//= require "jquery.stellar.min.js"
//= require "bootstrap.min.js"
//= require "price-regulator/jshashtable-2.1_src.js"
//= require "price-regulator/jquery.numberformatter-1.2.3.js"
//= require "price-regulator/tmpl.js"
//= require "price-regulator/jquery.dependClass-0.1.js"
//= require "price-regulator/draggable-0.1.js"
//= require "price-regulator/jquery.slider.js"
//= require "country.js"
//= require "masonry.pkgd.min.js"
//= require "morris.min.js"
//= require "raphael.min.js"
//= require "video.js"
//= require "selectBox.js"
//= require "blur.min.js"
//= require "main.js"

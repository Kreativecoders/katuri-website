require 'test_helper'

class KasturiControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get our_works" do
    get :our_works
    assert_response :success
  end

  test "should get about_us" do
    get :about_us
    assert_response :success
  end

  test "should get contact_us" do
    get :contact_us
    assert_response :success
  end

  test "should get services" do
    get :services
    assert_response :success
  end

  test "should get carrier" do
    get :carrier
    assert_response :success
  end

end
